<?php
include_once('class-credomatic-helper.php');

class WC_Baum_Payment_Gateway extends WC_Payment_Gateway {

	public $domain;
	private $logger;

	/**
	 * Constructor for the gateway.
	 */
	public function __construct() {
		$this->logger = new WC_Logger();
		$this->bli = new BPG_License_Init();
		$this->domain = 'baum-payment-gateway';
		$this->id = 'baum_payment_gateway';
		$this->transactionid = '';

		$this->method_title = __('BAC Credomatic by Baum Digital', $this->domain );
		$this->method_description = __('BAC Credomatic Payment Gateway developed by Baum Digital.', $this->domain );
		
		// The title to be used for the vertical tabs that can be ordered top to bottom
		$this->title = __('BAC Credomatic by Baum Digital', $this->domain );

		// Show an image next to the gateway's name on the frontend, enter a URL to an image.
		$this->icon = apply_filters('wc_baum_payment_gateway_icon', plugin_dir_url(__DIR__) . 'assets/images/baum-payment-gateway-icon.jpg');
		
		// Bool. Can be set to true if you want payment fields to show on the checkout if doing a direct integration, which we are doing in this case
		$this->has_fields = true;

		// This basically defines your settings which are then loaded with init_settings()
		$this->init_form_fields();
		// After init_settings() is called, you can get the settings and load them into variables, e.g: $this->title = $this->get_option('title');
		$this->init_settings();

		// Turn these settings into variables we can use
		foreach ( $this->settings as $setting_key => $value ) {
			$this->$setting_key = $value;
		}

		// Are we testing right now or is it a real transaction
		$this->test_mode = ( $this->environment == "yes" ) ? true : false;
		$this->browser_redirect = ( $this->transaction_method == 'browser_redirect' ) ? true : false;

		// Lets check for SSL
		add_action('admin_notices', array($this,  'do_ssl_check'));
		
		// Save settings
		if ( is_admin() ) {
			// Versions over 2.0
			// Save the administration options.
			add_action('woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options') );
		}

		// Actions
		add_action('woocommerce_order_details_after_order_table_items', array($this, 'baum_checkout_field_display_admin_order_meta'), 10, 1);

		add_filter('woocommerce_credit_card_form_fields' , array($this, 'baum_payment_gateway_extra_cc_fields') , 10, 2);

		add_action('wp_enqueue_scripts', array($this, 'baum_payment_gateway_script'));
		// add_filter('woocommerce_available_payment_gateways', array($this, 'baum_payment_gateway_enable_gateway_methods'));

		if($this->browser_redirect) {
			add_action('bpg_after_woocommerce_pay', array($this, 'baum_payment_gateway_browser_redirect_method'), 20, 1);			
			add_filter('woocommerce_locate_template', array($this, 'baum_payment_gateway_wc_locate_template'), 10, 3);
			add_filter('woocommerce_order_button_text', array($this, 'baum_payment_gateway_3d_checkout_button'));
		}
	}

	function payment_fields() {
		$baum_payment_gateway_cc = new WC_Payment_Gateway_CC();
		$baum_payment_gateway_cc->id = $this->id;
		$baum_payment_gateway_cc->supports = $this->supports;
		$description = (!is_wc_endpoint_url('order-pay')) ? $this->get_description() : '';

		if ( $description ) {
			if($this->test_mode) 
				$description .= ' <strong>Test Mode</strong>';
			echo wpautop(wptexturize($description));
		}

		if((!$this->browser_redirect && is_checkout()) || is_wc_endpoint_url('order-pay')) {
			// This method will call the default CC fields from WC
			$baum_payment_gateway_cc->form();
		}
	}

	/**
	 * Initialise Gateway Settings Form Fields.
	 */
	function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'   => __('Enable / Disable', $this->domain ),
				'type'    => 'checkbox',
				'label'   => __('Enable Payment Gateway', $this->domain ),
				'default' => 'no'
			),
			'title' => array(
				'title'       => __('Title', $this->domain ),
				'type'        => 'text',
				'desc_tip' => __('Payment title the customer will see during the checkout process.', $this->domain ),
				'default'     => __('Tarjeta Crédito/Débito', $this->domain ),
			),
			'description' => array(
				'title'       => __('Description', $this->domain ),
				'type'        => 'textarea',
				'desc_tip' => __('Payment method description that the customer will see on your checkout.', $this->domain ),
				'default'     => __('Pague seguro con sus tarjetas de crédito o débito.', $this->domain),
			),
			'order_status' => array(
				'title'       => __('Order Status', $this->domain ),
				'type'        => 'select',
				'class'       => 'wc-enhanced-select',
				'desc_tip' => __('Choose whether status you wish after checkout.', $this->domain ),
				'default'     => 'wc-pending',
				'options'     => wc_get_order_statuses(),
				'description' => __('Use <strong>Pending Payment</strong> method if you\'re using Browser Redirect method.', $this->domain ),
			),
			'trans_key' => array(
				'title'     => __('BAC Credomatic Transaction Key', $this->domain ),
				'type'      => 'password',
				'desc_tip'  => __('Transaction Key provided by BAC Credomatic.', $this->domain ),
			),
			'security_key_id' => array(
				'title'     => __('BAC Credomatic Security Key ID', $this->domain ),
				'type'      => 'text',
				'desc_tip'  => __('Security Key provided by BAC Credomatic.', $this->domain ),
			),
			'process_url' => array(
				'title'     => __('BAC Credomatic Process URL', $this->domain ),
				'type'      => 'text',
				'desc_tip'  => __('Action form URL provided by BAC Credomatic.', $this->domain ),
			),
			'transaction_type' => array(
				'title'     => __('Transaction Type', $this->domain ),
				'type'      => 'select',
				'class'       => 'wc-enhanced-select',
				'desc_tip'  => __('Transaction type.', $this->domain ),
				'default'     => 'sale',
				'description'  => __('Only available "sale" type.', $this->domain ),
				'options'     => array(
					'sale' => 'Sale',
					// 'auth' => 'Auth',
					// 'credit' => 'Credit'
				)
			),
			'transaction_method' => array(
				'title'   => __('Transaction Method', $this->domain ),
				'type'      => 'select',
				'class'       => 'wc-enhanced-select',
				'desc_tip'  => __('Transaction Method. Browser Redirect for 3D Secure.', $this->domain ),
				'default'     => 'envio_directo',
				'description' => __('Use <strong>form-pay.php</strong> located in the plugin folder if you\'re using Browser Redirect method.', $this->domain ),
				'options'     => array(
					'browser_redirect' => 'Browser Redirect'
				)
			),
			'environment' => array(
				'title'     => __('Test Mode', $this->domain ),
				'label'     => __('Enable Test Mode', $this->domain ),
				'type'      => 'checkbox',
				'desc_tip'  => __('Enable Test Mode to get data from response code 2.', $this->domain ),
				'description' => __('Place the payment gateway in test mode.', $this->domain ),
				'default'   => 'yes',
			),
			'instructions' => array(
				'title'       => __('Instructions', $this->domain ),
				'type'        => 'textarea',
				'desc_tip' => __('Instructions that will be added to the thank you page and emails.', $this->domain ),
				'default'     => 'Agregue los datos de su tarjeta.',
			),
		);
	}

	public function baum_checkout_field_display_admin_order_meta($order) {
		if ( !empty($this->transactionid) && $order->payment_method === $this->id ) {
			?>
			<tr class="baum-payment-gateway">
				<td class="td" scope="row" colspan="2">
					<strong class="title" style="text-align: center;"><?php echo $this->method_title; ?></strong><br />
					<span style="text-align: justify; font-size: 12px;"><?php sprintf(__('ID Transacción: %d. ', $this->domain), $this->transactionid); ?></span><br />
				</td>
			</tr>
			<?php
		}
	}

	public function baum_payment_gateway_extra_cc_fields($cc_fields, $payment_id) {
		global $woocommerce;

		$card_field = ($this->browser_redirect) ? 'ccnumber' : $payment_id . '-card-number';
		$expiry_field = ($this->browser_redirect) ? 'temp_ccexp' : $payment_id . '-card-expiry';
		$cvv_field = ($this->browser_redirect) ? 'cvv' : $payment_id . '-card-cvv';

		$endpoint = $woocommerce->query->get_current_endpoint();
		if (($this->browser_redirect && $endpoint == 'order-pay') || !$this->browser_redirect) {
			$cc_fields['card-number-field'] = '<p class="form-row form-row-wide">
			<label for="' . $payment_id . '-card-number">' . esc_html__( 'Card number', 'woocommerce' ) . '<span class="required">*</span></label>
			<input id="' . $payment_id . '-card-number" class="input-text wc-credit-card-form-card-number" inputmode="numeric" autocomplete="cc-number" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;" name="' . $card_field . '" required />
			</p>';

			$cc_fields['card-expiry-field'] = '<p class="form-row form-row-first">
			<label for="' . $payment_id . '-card-expiry">' . esc_html__( 'Caducidad',  $this->domain ) . ' (MM/YYYY)<span class="required">*</span></label>
			<input id="' . $payment_id . '-card-expiry" class="input-text wc-credit-card-form-card-expiry" inputmode="numeric" autocomplete="cc-exp" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="' . esc_attr__( 'MM / YYYY',  $this->domain ) . '" name="' . $expiry_field . '" required />
			</p>';

			$cc_fields['card-cvc-field'] = '<p class="form-row form-row-last">
			<label for="' . $payment_id . '-card-cvc">' . esc_html__( 'Card code', 'woocommerce' ) . '<span class="required">*</span></label>
			<input id="' . $payment_id . '-card-cvc" class="input-text wc-credit-card-form-card-cvc" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" maxlength="4" placeholder="' . esc_attr__( 'CVC', 'woocommerce' ) . '" name="' . $cvv_field . '" required style="width:100px" />
			</p>';

			$cc_fields['card-type'] = '<input type="hidden" name="' . $payment_id . '-card-type" class="wc-credit-card-form-card-type" />';
		} else {
			$cc_fields = [];
		}


		return $cc_fields;
	}

	/**
	 * Process the payment and return the result.
	 *
	 * @param int $order_id
	 * @return array
	 */
	public function process_payment($order_id) {
		if($this->browser_redirect) {
			$order = wc_get_order($order_id);
			$status = 'wc-' === substr( $this->order_status, 0, 3 ) ? substr( $this->order_status, 3 ) : $this->order_status;

			// Set order status
			$order->update_status($status, __('Checkout con ' . $this->method_title, $this->domain));

			wc_reduce_stock_levels($order_id);

			// Remove cart
			WC()->cart->empty_cart();

			// Return thankyou redirect
			return array(
				'result' 	=> 'success',
				'redirect' => $order->get_checkout_payment_url()
			);
		}
	}
	
	// Validate fields
	public function validate_fields() {
		return true;
	}
	
	// Check if we are forcing SSL on checkout pages
	// Custom function not required by the Gateway
	public function do_ssl_check() {
		if( $this->enabled == "yes" ) {
			if( get_option('woocommerce_force_ssl_checkout') == "no" ) {
				?>
				<div class="notice notice-error">
					<p><?php echo sprintf( __( "<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>" ), $this->method_title, admin_url('admin.php?page=wc-settings&tab=checkout') ); ?></p>
				</div>
				<?php
			}
		}       
	}

	function baum_payment_gateway_script() {
		wp_enqueue_style($this->id . '-style', plugin_dir_url(__DIR__) . 'assets/css/baum-payment-gateway.min.css');
		wp_enqueue_script($this->id . '-script', plugin_dir_url(__DIR__) . 'assets/js/baum-payment-gateway.min.js', array('jquery'), null, true);
		wp_localize_script($this->id . '-script', $this->id . '_ajax', array('ajax_url' => admin_url('admin-ajax.php'), 'nonce' => wp_create_nonce( "process_payment" ),));
	}

	/**
	** 3D Secure Support
	**/
	function baum_payment_gateway_wc_locate_template( $template, $template_name, $template_path ) {
		global $woocommerce;
		$_template = $template;

		if ( ! $template_path ) $template_path = $woocommerce->template_url;

		$plugin_path = untrailingslashit( plugin_dir_path( __DIR__ ) ) . '/woocommerce/';

		// Look within passed path within the theme - this is priority
		$template = locate_template(
			array(
				$template_path . $template_name,
				$template_name
			)
		);

		// Modification: Get the template from this plugin, if it exists
		if ( ! $template && file_exists( $plugin_path . $template_name ) )
			$template = $plugin_path . $template_name;

		// Use default template
		if ( ! $template )
			$template = $_template;

		// Return what we found
		return $template;
	}

	public function baum_payment_gateway_browser_redirect_method($order_id) {
		global $woocommerce;

		$customer_order = wc_get_order($order_id);
		
		if($customer_order->get_payment_method() == $this->id) {
			$credomatic_helper = new Credomatic_Helper();

			$order_number = str_replace( "#", "", $customer_order->get_order_number() );
			$time = time();
			$order_total = $customer_order->get_total();

			$helper_data = array(
				'orderid' => $order_id,
				'amount' => $order_total,
				'time' => $time
			);

			$inbound = $this->bli->bpg_exec_create_inbound($helper_data);
			if(empty($inbound->hash) && !empty($inbound)) {
				$this->log(sprintf("Error generando hash: %s ...", $inbound));
			} else {
				$hash = $inbound->hash;
			}

			if(isset($_GET['response'])) {
				$credomatic = $credomatic_helper->processResponse($_GET, $this->bli);
				$response_code = $_GET['response'];

				$response_text = $credomatic['text'];
				$response_message = $credomatic['message'];
				$response_authcode = $_GET['authcode'];
				$response_transactionid = $_GET['transactionid'];
				$response_avsresponse = $_GET['avsresponse'];
				$response_cvvresponse = $_GET['cvvresponse'];
				$response_response_code = $_GET['response_code'];
				$response_username = $_GET['username'];
				$response_orderid = $_GET['orderid'];
				$response_type = $_GET['type'];
				$response_amount = $_GET['amount'];
				$response_hash = $_GET['hash'];
			}

			// response=2&responsetext=Transaction+received+but+declined&authcode=&transactionid=3883692771&avsresponse=U&cvvresponse=P&orderid=3211&type=sale&response_code=200&username=8847252&time=1511822451&amount=14500.00&hash=626cd33f37c1336f8c9b0d3f7940d17b

			$cards_image = apply_filters('bpg_cards_image_tag', '<img src="' . $this->icon . '" alt="' . $this->id . '">');
			?>
			<form id="payment" data-id="<?= get_the_ID(); ?>" name="CredomaticPost" method="post" action="<?php echo $this->process_url ?>" class="order-payment-form">
				<?php
					if ( $instructions = $this->instructions ) {
						if($this->test_mode) 
							$instructions .= ' <strong>Test Mode</strong>';
						echo wpautop( wptexturize( $instructions ) );
					}

					echo $cards_image;
				?>
				<div class="payment_box show">
					<?php
						$this->payment_fields();
					?>
					<input type="hidden" name="ccexp" placeholder="ccexp" value="">
					<input type="hidden" name="hash" value="<?= (!empty($hash)) ? $hash : 0 ?>">
					<input type="hidden" name="time" value="<?= $time ?>">
					<input type="hidden" name="amount" value="<?= $order_total ?>">
					<input type="hidden" name="type" value="sale">
					<input type="hidden" name="orderid" value="<?= $order_id ?>">
					<input type="hidden" name="key_id" value="<?php echo $this->security_key_id ?>">
					<input type="hidden" name="address" value="">
					<input type="hidden" name="redirect" value="<?php echo $customer_order->get_checkout_payment_url() ?>">
	                <button type="submit" name="submit_credomatic" id="submit-credomatic" class="button btn-credomatic"><?php echo apply_filters( 'baum_payment_gateway_pay_button_text', __('Pagar', $this->domain)) ?></button>
				</div>
				<div class="payment_result"></div>
			</form>
			<?php
			if(empty($this->process_url)) {
				wc_add_notice(__('La transacción no se puede efectuar por falta de credenciales del BAC.', $this->domain), 'notice');
			}

			if(isset($response_code)) {
				$response_valid = $this->bli->bpg_exec_check_response($_GET);

				if(!$response_valid['valid']) {
					$this->log(sprintf("Error checking hash: %s ...", $response_valid['message']));
				}
				
				global $wpdb;

				$table_bitacora = $wpdb->prefix . 'bpg_bitacora_transacciones';
				$data = array(
					'persona' => 1,
					'monto' => isset($response_amount) ?  $response_amount :  0.00,
					'orden' => isset($response_orderid) ?  $response_orderid :  '',
					'metodo_pago' => isset($response_type) ?  $response_type :  '',
					'fecha_inicio' => date('Y-m-d H:m:i'),
					'estado' => $credomatic['code'] == 1 ? 1 : 0,
					'authcode' => isset($credomatic['authcode']) ? $credomatic['authcode'] : '',
					'transactionid' => isset($credomatic['transactionid']) ? $credomatic['transactionid'] : '',
					'fecha_fin' => date('Y-m-d H:m:i'),
					'error' => (!$credomatic['valid']) ? json_encode($credomatic) : ''
				);
				$wpdb->insert( $table_bitacora, $data );
				$wpdb->flush();

				if (($response_code == 1) || ($this->test_mode && $response_code == 2)) {
					if($this->test_mode) {
						$customer_order->add_order_note(__('Proceso de pago finalizado en modo prueba activado.', $this->domain));
					} else {
						$customer_order->add_order_note(__('Proceso de pago finalizado. ', $this->domain));
					}

					$order = wc_get_order($order_id);
					$status = 'wc-' === substr( $this->order_status, 0, 3 ) ? substr( $this->order_status, 3 ) : $this->order_status;

					$order->update_status( $status, __('Checkout con ' . $this->method_title, $this->domain ) );

					update_post_meta($order_id, '_payment_method', $this->id);
					update_post_meta($order_id, '_payment_method_title', $this->title);
					update_post_meta($order_id, $this->id . '_transactionid', $response_transactionid);
					
					$customer_order->payment_complete();

					do_action('baum_payment_gateway_after_submit_form', $order_id);

					wp_redirect($this->get_return_url($order));
					exit(); 
				} elseif ($response_code == 2) {
					wc_add_notice($response_text . '. ' . $response_message, 'notice');
					$customer_order->add_order_note($response_text);
				} else {
					$customer_order->add_order_note('Error: '. $response_text . '. ' . $response_message);
					wc_add_notice($response_text . '. ' . $response_message, 'error');

					$this->log(sprintf("POST with errors=%s ...", $response_text));
				}
			}
		}
	}

	function baum_payment_gateway_3d_checkout_button() {
		return apply_filters('bpg_checkout_button_text', __('Continuar con el pago', $this->domain));
	}

	public function log($message) {
		$this->logger->add($this->id, $message);
	}
}
