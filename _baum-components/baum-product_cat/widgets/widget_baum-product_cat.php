<?php
/**
** Widget Baum Product Categories
** To Do: Call this file in /functions/wp-widgets.php
** then add register_widget('WC_Baum_Product_Categories') to action widgets_init
**
** Extension based on WC Products Category widget to show WC Products categories based on parent category.
** v1.0
**/
class WC_Baum_Product_Categories extends WC_Widget {
	/**
	** Constructor.
	**/
	public function __construct() {
		$this->widget_cssclass    = 'wc_baum_widget widget_product_categories';
		$this->widget_description = __('Muestra categorías de productos según un padre específico.', 'baumchild');
		$this->widget_id          = 'wc_baum_product_cat';
		$this->widget_name        = __( 'Baum Categoria de Productos', 'baumchild' );

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @see WP_Widget
	 * @param array $args     Widget arguments.
	 * @param array $instance Widget instance.
	 */
	public function widget( $args, $instance ) {
		global $wp_query, $post;

		$cur_parent_cat = isset( $instance['parent_cat'] ) ? $instance['parent_cat'] : -1;
		$count = isset( $instance['count'] ) ? $instance['count'] : '';
		$hierarchical = isset( $instance['hierarchical'] ) ? $instance['hierarchical'] : '';
		$hide_empty = isset( $instance['hide_empty'] ) ? $instance['hide_empty'] : 0;
		$orderby = isset( $instance['orderby'] ) ? $instance['orderby'] : '';
		$max_depth = isset( $instance['max_depth'] ) ? $instance['max_depth'] : 0;
		
		$list_args = array(
			'show_count'   => $count,
			'hierarchical' => $hierarchical,
			'taxonomy'     => 'product_cat',
			'hide_empty'   => $hide_empty,
		);
		$list_args['menu_order'] = false;
		$list_args['depth']      = $max_depth;

		if ( 'name' === $orderby ) {
			$list_args['orderby'] = 'title';
		} else {
			$list_args['menu_order'] = str_replace('order_', '', $orderby);
		}

		if($cur_parent_cat != -1) {
			$list_args['child_of'] = $cur_parent_cat;
		}

		$this->widget_start( $args, $instance );

		include_once WC()->plugin_path() . '/includes/walkers/class-wc-product-cat-list-walker.php';

		$list_args['walker']                     = new WC_Product_Cat_List_Walker();
		$list_args['title_li']                   = '';
		$list_args['pad_counts']                 = 1;
		$list_args['show_option_none']           = __( 'No existen categorías de producto.', 'baumchild' );
		$list_args['max_depth']                  = $max_depth;

		echo '<ul class="baum-product-categories menu">';

		wp_list_categories( apply_filters( 'woocommerce_product_categories_widget_args', $list_args ) );

		echo '</ul>';

		$this->widget_end( $args );
	}

	public function update( $new_instance, $old_instance ) {
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['parent_cat'] = sanitize_text_field( $new_instance['parent_cat'] );
		$instance['hide_empty'] = sanitize_text_field( $new_instance['hide_empty'] );
		$instance['hierarchical'] = sanitize_text_field( $new_instance['hierarchical'] );
		$instance['count'] = sanitize_text_field( $new_instance['count'] );
		$instance['orderby'] = sanitize_text_field( $new_instance['orderby'] );
		$instance['max_depth'] = sanitize_text_field( $new_instance['max_depth'] );

		return $instance;
	}

	public function form( $instance ) {
		$cur_parent_cat = isset( $instance['parent_cat'] ) ? esc_attr( $instance['parent_cat'] ) : -1;
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$hide_empty = isset( $instance['hide_empty'] ) ? esc_attr( $instance['hide_empty'] ) : 0;
		$hierarchical = isset( $instance['hierarchical'] ) ? esc_attr( $instance['hierarchical'] ) : 0;
		$count = isset( $instance['count'] ) ? esc_attr( $instance['count'] ) : 0;
		$orderby = isset( $instance['orderby'] ) ? esc_attr( $instance['orderby'] ) : '';
		$max_depth = isset( $instance['max_depth'] ) ? esc_attr( $instance['max_depth'] ) : '';
		$parent_cats = get_terms(['taxonomy' => 'product_cat', 'hide_empty' => false, 'parent' => 0]);
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo __('Título', 'baumchild'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<input class="widefat" id="<?php echo $this->get_field_id( 'hide_empty' ); ?>" name="<?php echo $this->get_field_name( 'hide_empty' ); ?>" type="checkbox" value="1" <?php checked($hide_empty, '1'); ?>/>
			<label for="<?php echo $this->get_field_id( 'hide_empty' ); ?>"><?php echo __('Ocultar categorías vacías', 'baumchild'); ?></label>
		</p>
		<p>
			<input class="widefat" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" type="checkbox" value="1" <?php checked($count, '1'); ?>/>
			<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php echo __('Mostrar contador de productos', 'baumchild'); ?></label>
		</p>
		<p>
			<input class="widefat" id="<?php echo $this->get_field_id( 'hierarchical' ); ?>" name="<?php echo $this->get_field_name( 'hierarchical' ); ?>" type="checkbox" value="1" <?php checked($hierarchical, '1'); ?>/>
			<label for="<?php echo $this->get_field_id( 'hierarchical' ); ?>"><?php echo __('Mostar jerarquía', 'baumchild'); ?></label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('orderby'); ?>"><?php echo __('Ordenar por', 'baumchild') ?>
				<select class="widefat" name="<?php echo $this->get_field_name( 'orderby' ); ?>" id="<?php echo $this->get_field_id('orderby'); ?>">
					<option value="order_asc" <?php selected($orderby, 'order_asc') ?>><?php echo __('Orden ascendente', 'baumchild') ?></option>
					<option value="order_desc" <?php selected($orderby, 'order_desc') ?>><?php echo __('Orden descendente', 'baumchild') ?></option>
					<option value="name" <?php selected($orderby, 'name') ?>><?php echo __('Nombre', 'baumchild') ?></option>
				</select>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('parent_cat'); ?>"><?php echo __('Mostrar hijas de', 'baumchild') ?>
				<select class="widefat" name="<?php echo $this->get_field_name( 'parent_cat' ); ?>" id="<?php echo $this->get_field_id('parent_cat'); ?>">
					<option value="-1"><?php echo __('Show all', 'baumchild') ?></option>
					<?php foreach ($parent_cats as $parent_cat) : ?>
						<option value="<?= $parent_cat->term_id ?>" <?php selected($parent_cat->term_id, $cur_parent_cat) ?>><?= $parent_cat->name ?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'max_depth' ); ?>"><?php echo __('Profundidad máxima', 'baumchild'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'max_depth' ); ?>" name="<?php echo $this->get_field_name( 'max_depth' ); ?>" type="text" value="<?php echo $max_depth; ?>" />
		</p>
		<?php
	}
}