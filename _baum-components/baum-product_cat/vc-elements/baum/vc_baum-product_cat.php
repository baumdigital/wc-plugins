<?php
/**
** VC Baum Sociales
** Call this file in /functions/vc-elements/custom-elements.php
** then call function VC_BAUM_PROD_CAT() in __construct()
**
** Requires function baumchild_sociales to get all sociales. Extension to show custom Baum Sociales from Customizer options.
** v1.0.2
**/
class VC_Baum_Product_Category {
	function __construct() {
		// Element Mapping
        add_action('init', array($this, 'vc_baum_categories_mapping'));
        
		// Element HTML
        add_shortcode('vc_baum_categories', array($this, 'vc_baum_categories_html'));
	}

    /**
    ** Custom element to display WC Categories based on Baum Widget
    **/
    public function vc_baum_categories_mapping() {
        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) { return; }

        $the_categories = array();
        $get_categories = get_categories(array(
            'taxonomy' => 'product_cat',
            'hide_empty' => false,
            'depth' => 1
        ));

        if ($get_categories) {
            foreach ($get_categories as $category) {
                $the_categories[$category->cat_name] = $category->term_id;
            }
        }

        vc_map(
            array(
                'name' => __('Baum Categoría de Productos', 'baumchild'),
                'base' => 'vc_baum_categories',
                'description' => __('Muestra las subcategoría por categoría padre', 'baumchild'),
                'category' => __('Baum Themes', 'baumchild'),
                'icon' => get_stylesheet_directory_uri() . '/functions/vc-elements/images/placeholder.jpg',
                'params' => array(
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Categoría', 'baumchild'),
                        'param_name' => 'term_id',
                        'value' => $the_categories,
                        'save_always' => true,
                        'admin_label' => true,
                        'description' => __('Seleccione la categoría padre', 'baumchild')
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Ocultar vacías', 'baumchild'),
                        'param_name' => 'hide_empty',
                        'admin_label' => true,
                        'description' => __('Oculta las categorías vacías', 'baumchild')
                    )
                )
            )
        );
    }

    public function vc_baum_categories_html($atts) {
        // Params extraction
        extract( $atts );
        $new_list_args = array(
            'hierarchical' => true,
            'taxonomy' => 'product_cat',
            'hide_empty' => (bool) $atts['hide_empty'],
            'parent' => $atts['term_id'],
            'max_depth' => 3
        );
        $terms_id = array();
        $categories = get_categories($new_list_args);
        
        $term = get_term($atts['term_id'], 'product_cat');
        $thumbnail_id = get_term_meta($term->term_id, 'thumbnail_id', true); // get_woocommerce_term_meta
        $image = wp_get_attachment_url($thumbnail_id);
        ?>
        
        <header class="vc_wc-product-categories-header">
            
            <?php if (!empty($thumbnail_id)) : ?>
                <img src="<?= $image ?>" class="category-image" alt="<?= $parent_cat->name ?>">
            <?php endif; ?>

            <h1><?= $term->name ?></h1>
        </header>

        <div class="vc_wc-product-categories-content white_content">
            <?php
                if( $categories ) {
                    foreach ($categories as $key => $parent_cat) {
                        $term_id = $parent_cat->term_id;
                        $args['child_of'] = $term_id;
                        $args['current_category'] = $term_id;

                        include_once WC()->plugin_path() . '/includes/walkers/class-wc-product-cat-list-walker.php';

                        $args['walker'] = new WC_Product_Cat_List_Walker();
                        $args['hierarchical'] = $new_list_args['hierarchical'];
                        $args['taxonomy'] = $new_list_args['taxonomy'];
                        $args['hide_empty'] = $new_list_args['hide_empty'];
                        $args['title_li'] = '';
                        $args['pad_counts'] = 1;
                        $args['show_option_none'] = '';
                        $args['max_depth'] = $new_list_args['max_depth'];
                        ?>
                        <div id="cat_<?= $term_id ?>" class="vc_wc-product-categories-wrapper">
                            <p class="vc_wc-product-categories-title"><?= $parent_cat->cat_name ?></p>
                            <ul class="vc_wc-product-categories menu">
                                <?php wp_list_categories( apply_filters( 'woocommerce_product_categories_widget_args', $args ) ); ?> 
                                
                            </ul>
                        </div>
                        <?php
                        // foreach ($parent_cat->term_id as $key => $_term_id) {
                        // }
                    }
                } else {
                    ?>
                    <p class="mb-0 text-center"><?= __('No hay categorías disponibles', 'baumchild') ?></p>
                    <?php
                }
            ?>
        </div>
        <?php

        return ob_get_clean();
    }
}

function vc_baum_prod_cat() {
	if ( !class_exists( 'WC_Baum_Product_Categories' ) ) 
		return;

	return new VC_Baum_Product_Category();
}
