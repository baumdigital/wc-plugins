## Documentación de Componentes ##

## GENERAL ##
# TODOS los directorios de componentes están orientados para su uso dentro de la carpeta functions que debe tener cada child theme creado por Baum
# Se debe copiar su contenido dentro de esta carpeta y mantener la distribución que cada uno lleva
# Hacer el llamado únicamente del archivo requerido, seguir los pasos para incluir los componentes según se requiera: Widget, Shortcode, Visual Composer component, etc.



## CHANGELOG - Baum Sociales

## CHANGELOG - Baum Blurb
# v1.0.3 - 23-11-2020
* Fix: Validación para quitar los warnings del WP_DEBUG

# v1.0.2
* Fix: Posicionamiento de íconos y selección de tipo Blurb cuando se agrega un ícono nuevo

# v1.0.1
* Fix: Uso del text editor en lugar de título y extracto
* New: Icon size en pixels
* New: Custom wrapper class
* Fix href validation

## CHANGELOG - Baum Sociales
# v1.0.2
* Update: Unificación de tags para mostrar los sociales, se quedan como <span class="baumchild_social baumchild_social-{id}"></span> en wp-baum-sociales.php

# v1.0.1
* Initial

## CHANGELOG - Baum Logs
# v1.0.1 - Belanú
* Nombre genérico para la clase
* Uso de date_i18n para que detecte la zona horaria

# v1.0.0
* Initial
