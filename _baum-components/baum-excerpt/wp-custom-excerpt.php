<?php
/**
** Excerpt like Divi
**/
function baumchild_excerpt($length = 120) {
	return wpautop( _delete_post_first_video( strip_shortcodes( _truncate_post( $length, false, '', true ) ) ) );
}

function _delete_post_first_video( $content ) {
	if ( 'video' === get_post_format() ) {
		preg_match_all( '|^\s*https?:\/\/[^\s"]+\s*|im', $content, $urls );

		if ( ! empty( $urls[0] ) ) {
			$content = str_replace( $urls[0], '', $content );
		}
	}

	return $content;
}

function _truncate_post( $amount, $echo = true, $post = '' ) {
	if ( '' == $post ) {
		global $post;
	}

	if ( post_password_required( $post ) ) {
		$post_excerpt = get_the_password_form();

		if ( $echo ) {
			echo $post_excerpt;
			return;
		}

		return $post_excerpt;
	}

	$post_excerpt = apply_filters( 'the_excerpt', $post->post_excerpt );

	// get the post content
	$truncate = $post->post_content;

	// remove caption shortcode from the post content
	$truncate = preg_replace('@\[caption[^\]]*?\].*?\[\/caption]@si', '', $truncate);

	/**
	 * Filter automatically generated post excerpt before it gets truncated.
	 *
	 * @since 2.17
	 *
	 * @param string $excerpt
	 * @param integer $post_id
	 */
	// $truncate = apply_filters( 'et_truncate_post', $truncate, $post->ID );

	// apply content filters
	$truncate = apply_filters( 'the_content', $truncate );

	// decide if we need to append dots at the end of the string
	if ( strlen( $truncate ) <= $amount ) {
		$echo_out = '';
	} else {
		$echo_out = ' [...]';
		// $amount = $amount - 3;
	}

	// trim text to a certain number of characters, also remove spaces from the end of a string ( space counts as a character )
	$truncate = rtrim( _wp_trim_words( $truncate, $amount, '' ) );

	// remove the last word to make sure we display all words correctly
	if ( '' != $echo_out ) {
		$new_words_array = (array) explode( ' ', $truncate );
		array_pop( $new_words_array );

		$truncate = implode( ' ', $new_words_array );

		// append dots to the end of the string
		$truncate .= $echo_out;
	}

	if ( $echo ) {
		echo $truncate;
	} else {
		return $truncate;
	}
}

function _wp_trim_words( $text, $num_words = 55, $more = null ) {
	if ( null === $more )
		$more = esc_html__( '&hellip;' );
	$original_text = $text;
	$text = wp_strip_all_tags( $text );

	$text = trim( preg_replace( "/[\n\r\t ]+/", ' ', $text ), ' ' );
	preg_match_all( '/./u', $text, $words_array );
	$words_array = array_slice( $words_array[0], 0, $num_words + 1 );
	$sep = '';

	if ( count( $words_array ) > $num_words ) {
		array_pop( $words_array );
		$text = implode( $sep, $words_array );
		$text = $text . $more;
	} else {
		$text = implode( $sep, $words_array );
	}

	return apply_filters( 'wp_trim_words', $text, $num_words, $more, $original_text );
}