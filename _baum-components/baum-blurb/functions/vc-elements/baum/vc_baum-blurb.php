<?php
/** 
 * VC Baum Blurb
 * Call this file in /functions/vc-elements/custom-elements.php
 * then call function VC_BAUM_BLURB() in __construct()
 *
 * Blurb functionality like Divi Blurb
 * @version 1.0.4
 */
class VC_Baum_Blurb {
	function __construct() {
		// Element Mapping
		add_action('init', array($this, 'vc_baum_blurb_mapping'));
		
		// Element HTML
		add_shortcode('vc_baum_blurb', array($this, 'vc_baum_blurb_html'));

		// CSS
		add_action( 'vc_base_register_front_css', array( $this, 'vc_baum_register_font_awesome_css' ) );
	}

	/**
	 * Custom Blurb for VC
	 * @since 1.0.0
	 */	
	public function vc_baum_blurb_mapping() {
		// Stop all if VC is not enabled
		if (!defined('WPB_VC_VERSION')) { return; }

		// Map the block with vc_map()
		vc_map(
			array(
				'name' => __('Baum VC Blurb', 'baumchild'),
				'base' => 'vc_baum_blurb',
				'description' => __('Agrega un elemento de imagen y texto', 'baumchild'), 
				'icon' => get_stylesheet_directory_uri() . '/functions/vc-elements/images/placeholder.jpg',
				"category" => __('Baum Themes', 'baumchild'),          
				'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => __('Tipo', 'baumchild'),
						'param_name' => 'blub_type',
						'description' => __('Usuarios, Imagen o Ícono', 'baumchild'),
						'value' => array(
							esc_html__('Usuario', 'baumchild') => 'user',
							esc_html__('Imagen', 'baumchild') => 'image',
							esc_html__('Ícono', 'baumchild') => 'icon'
						),
						'std' => 'image',
						'weight' => 1,
						'admin_label' => true
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Librería de íconos', 'baumchild'),
						'value' => array(
							esc_html__( 'Font Awesome 5', 'baumchild' ) => 'fontawesome'
						),
						'param_name' => 'icon_type',
						'dependency' => array(
							'element' => 'blub_type',
							'value' => array( 'icon' ),
						),
						'std' => 'fontawesome',
						'weight' => 1,
						'description' => esc_html__( 'Seleccione la librería de íconos.', 'baumchild' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => esc_html__('Ícono', 'baumchild'),
						'param_name' => 'icon_fontawesome',
						'value' => 'fas fa-adjust',
						'settings' => array(
							'emptyIcon' => false,
							// default true, display an "EMPTY" icon?
							'iconsPerPage' => 500,
							// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value' => 'fontawesome',
						),
						'weight' => 1,
						'description' => esc_html__('Seleccione un ícono.', 'baumchild'),
					),
					array(
						'type' => 'attach_image',
						'class' => 'image-class',
						'heading' => __('Imagen', 'baumchild'),
						'param_name' => 'image',
						'dependency' => array(
							'element' => 'blub_type',
							'value' => array( 'user', 'image' ),
						),
						'description' => __('Imagen del Blurb. Úsese también para sobreescribir la foto del usuario usando el blurb de tipo Usuario.', 'baumchild')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Usuario', 'baumchild'),
						'param_name' => 'blurb_user',
						'description' => __('Seleccione un usuario', 'baumchild'),
						'value' => $this->vc_baum_blurb_get_users(),
						'dependency' => array(
							'element' => 'blub_type',
							'value' => array( 'user' ),
						),
						'admin_label' => false
					),
					array(
						"type" => "textarea_html",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Content", "baumchild" ),
						"param_name" => "content",
						"value" => __( "<p>Este es el bloque de edición. Clic en editar para agregar contenido.</p>", "baumchild" ),
						"description" => __( "Agregue el contenido a mostrar en el Blurb. Úsese también sobreescribir la descripción de usuario usando el blurb de tipo Usuario.", "baumchild" )
					),
					array(
						'type' => 'textfield',
						'heading' => __('Campos del usuario', 'baumchild'),
						'param_name' => 'blurb_user_fields',
						'dependency' => array(
							'element' => 'blub_type',
							'value' => array( 'user' ),
						),
						'description' => sprintf( __('Agregue campos custom del usuario al Blurb, separados por coma. Disponibles: <ul><li>%s</li></ul>', 'baumchild'), implode( '</li><li>', $this->vc_baum_blurb_user_fields() ) )
					),
					array(
						'type' => 'textfield',
						'heading' => __('Wrapper classes', 'baumchild'),
						'param_name' => 'blurb_wrapper_css',
						'description' => __('Clases custom al contenedor del blurb', 'baumchild')
					),
					// Media
					array(
						'type' => 'dropdown',
						'class' => 'media_position',
						'heading' => __('Posición', 'baumchild'),
						'param_name' => 'media_position',
						'value' => array(
							esc_html__('Default', 'baumchild') => '',
							esc_html__('Left', 'baumchild') => 'flex-row',
							esc_html__('Right', 'baumchild') => 'flex-row-reverse',
							esc_html__('Top', 'baumchild') => 'flex-column'
						),
						'admin_label' => true,
						'group' => esc_html__('Media', 'baumchild'),
						'description' => __('Posición del Media. Use <strong>Default</strong> para posicionarlo con CSS.', 'baumchild')
					),
					array(
						'type' => 'dropdown',
						'class' => 'media_align',
						'heading' => __('Vertical align', 'baumchild'),
						'param_name' => 'media_align',
						'dependency' => array(
							'element' => 'media_position',
							'value' => array('flex-row', 'flex-row-reverse'),
						),
						'admin_label' => true,
						'value' => array(
							esc_html__('Default', 'baumchild') => '',
							esc_html__('Top', 'baumchild') => 'align-items-start',
							esc_html__('Middle', 'baumchild') => 'align-items-center',
							esc_html__('End', 'baumchild') => 'align-items-end'
						),
						'group' => esc_html__('Media', 'baumchild'),
						'description' => __('Alineamiento vertical. Use <strong>Default</strong> para alinearlo con CSS', 'baumchild')
					),
					array(
						'type' => 'dropdown',
						'class' => 'media_text_align',
						'heading' => __('Text align', 'baumchild'),
						'param_name' => 'media_text_align',
						'dependency' => array(
							'element' => 'media_position',
							'value' => array('flex-column', 'flex-row', 'flex-row-reverse'),
						),
						'value' => array(
							esc_html__('Default', 'baumchild') => '',
							esc_html__('Left', 'baumchild') => 'text-left',
							esc_html__('Center', 'baumchild') => 'text-center',
							esc_html__('Right', 'baumchild') => 'text-right',
							esc_html__('Justify', 'baumchild') => 'text-justify'
						),
						'group' => esc_html__('Media', 'baumchild'),
						'description' => __('Alineación del texto. Use <strong>Default</strong> para alinear el texto con CSS', 'baumchild')
					),
					// Style
					array(
						'type' => 'colorpicker',
						'class' => 'title-color-class',
						'heading' => __('Color de ícono', 'baumchild'),
						'param_name' => 'icon_color',
						'admin_label' => true,
						'dependency' => array(
							'element' => 'blub_type',
							'value' => array('icon'),
						),
						'group' => esc_html__('Style', 'baumchild'),
						'description' => __('Color de ícono', 'baumchild')
					),
					array(
						'type' => 'colorpicker',
						'class' => 'bg-color-class',
						'heading' => __('Fondo', 'baumchild'),
						'param_name' => 'bg_color',
						'admin_label' => true,
						'group' => esc_html__('Style', 'baumchild'),
						'description' => __('Color de fondo de imagen/ícono', 'baumchild')
					),
					array(
						'type' => 'dropdown',
						'value' => array(
							__('None', 'baumchild') => '',
							__('Mini 60x60', 'baumchild') => 'bmn',
							__('Small 70x70', 'baumchild') => 'bsm',
							__('Normal', 'baumchild') => 'bnm',
							__('Large', 'baumchild') => 'blg',
							__('Extra Large', 'baumchild') => 'bxl',
						),
						'class' => 'blurb_size',
						'heading' => __('Tamaño', 'baumchild'),
						'param_name' => 'blurb_size',
						'admin_label' => true,						
						'dependency' => array(
							'element' => 'blub_type',
							'value' => array('image'),
						),
						'group' => esc_html__('Style', 'baumchild'),
						'description' => __('Tamaño de imagen del Blurb dado por la clase seleccionada', 'baumchild')
					),
					array(
						'type' => 'textfield',
						'class' => 'blurb_icon_size',
						'heading' => __('Tamaño seas', 'baumchild'),
						'param_name' => 'blurb_icon_size',
						'admin_label' => true,
						'dependency' => array(
							'element' => 'blub_type',
							'value' => array('usuarios'),
						),
						'group' => esc_html__('Style', 'baumchild'),
						'description' => __('Tamaño del ícono en pixeles', 'baumchild')
					),
					array(
						'type' => 'textfield',
						'class' => 'blurb_icon_size',
						'heading' => __('Tamaño', 'baumchild'),
						'param_name' => 'blurb_icon_size',
						'admin_label' => true,
						'dependency' => array(
							'element' => 'blub_type',
							'value' => array('icon'),
						),
						'group' => esc_html__('Style', 'baumchild'),
						'description' => __('Tamaño del ícono en pixeles', 'baumchild')
					),
					array(
						'type' => 'dropdown',
						'value' => array(
							__('Square', 'baumchild') => 'square',
							__('Circle', 'baumchild') => 'circle'
						),
						'class' => 'blurb_style',
						'heading' => __('Style', 'baumchild'),
						'param_name' => 'blurb_style',
						'admin_label' => false,
						'group' => esc_html__('Style', 'baumchild'),
						'description' => __('Estilo del Blurb.', 'baumchild')
					),
					// Enlace
					array(
						'type' => 'dropdown',
						'value' => array(
							__('Sin enlace', 'baumchild') => 'no_link',
							__('Botón', 'baumchild') => 'link_button',
							__('Enlace de autor (Blurb de usuario)', 'baumchild') => 'link_author',
							__('Ícono / Imagen', 'baumchild') => 'link_icon_image',
							__('Elemento Blurb', 'baumchild') => 'link_blurb'
						),
						'class' => 'link_type',
						'heading' => __('Tipo de enlace', 'baumchild'),
						'param_name' => 'link_type',
						'admin_label' => true,
						'group' => esc_html__('Enlace', 'baumchild'),
						'description' => __('Configuraciones para el enlace del Blurb.', 'baumchild')
					),
					array(
						'type' => 'vc_link',
						'class' => 'link-class',
						'heading' => __('Enlace', 'baumchild'),
						'admin_label' => false,
						'dependency' => array(
							'element' => 'link_type',
							'value' => array( 'link_button', 'link_icon_image', 'link_blurb' ),
						),
						'param_name' => 'link',
						'group' => esc_html__('Enlace', 'baumchild'),
						'description' => __('Enlace del ícono/image del blurb', 'baumchild')
					),
					array(
						'type' => 'textfield',
						'class' => 'button-link-text',
						'heading' => __('Texto del botón', 'baumchild'),
						'admin_label' => false,
						'dependency' => array(
							'element' => 'link_type',
							'value' => array('link_button', 'link_author'),
						),
						'param_name' => 'button_link_text',
						'group' => esc_html__('Enlace', 'baumchild'),
						'description' => __('Escriba el texto a mostrar en el botón, en enlace es el mismo del campo anterior.', 'baumchild')
					),
					array(
						'type' => 'textfield',
						'class' => 'button_classes',
						'heading' => __('Clases botón', 'baumchild'),
						'admin_label' => false,
						'dependency' => array(
							'element' => 'link_type',
							'value' => array('link_button', 'link_author'),
						),
						'param_name' => 'button_classes',
						'group' => esc_html__('Enlace', 'baumchild'),
						'value' => 'btn vc_btn-baum-primary',
						'description' => __('Agregue las clases del botón.', 'baumchild')
					)
				)
			)
		);
	}

	/**
	 * Blurb HTML
	 * Use the filter vc_baum_blurb_wrapper to override this HTML
	 * 
	 * @since 1.0.0
	 */
	public function vc_baum_blurb_html( $atts, $content = null ) {
		$media_position = $media_align = $media_text_align = $blurb_wrapper_css = $image_size = $icon_type = $user_avatar = $blub_type = $link = $link_type = $blurb_styles_tag = $icon_styles_tag = $image = $user_avatar = $icon_class = '';
		$href_attrs = $defaults = array();
		// Params extraction
		extract( $atts );
		$blub_classes = array('media-wrapper');
		$blurb_styles = $icon_styles = array();

		if ( !empty($bg_color) ) {
			$blurb_styles[] = 'background-color:' . $bg_color;
		}
		if ( !empty($icon_color) ) {
			$icon_styles[] = 'color:' . $icon_color;
		}
		if ( !empty($blurb_icon_size) ) {
			$pixeles = ltrim(str_replace(array('px', 'em', 'rem'), '', $blurb_icon_size));
			if (is_numeric($pixeles))
				$icon_styles[] = 'font-size:' . $pixeles . 'px';
		}
		if ( isset($blurb_size) && !empty($blurb_size) ) {
			$image_size = ($blurb_size == 'bmn' || $blurb_size == 'bsm') ? 'thumbnail' : 'full';
			$blub_classes[] = 'size-' . $blurb_size;
		}
		$custom_classes = !empty( $media_position ) ? array('d-flex', 'justify-content-center', $media_position, $media_align, $media_text_align) : array( 'custom-baum-blurb' );
		$custom_classes[] = !empty( $blurb_wrapper_css ) ? $blurb_wrapper_css : '';
		// $custom_classes[] = !empty( $image ) ? 'blurb-image' : 'blurb-icon';
		$custom_classes[] = !empty( $image ) ? 'blurb-image' : 'blurb-' . $blub_type;

		if ( isset( $blurb_style ) ) {
			$custom_classes[] = 'blurb-' . $blurb_style;
		}
			
		if ( !empty( $blurb_styles ) ) {
			$blurb_styles_tag = 'style="' . implode('; ', $blurb_styles) . '"';
		}
			
		if ( !empty( $icon_styles ) ) {
			$icon_styles_tag = 'style="' . implode('; ', $icon_styles) . '"';
		}
		
		// Link está activo y enlace no está vacío
		if ( !empty( $link ) && $link_type != 'no_link' ) {
			$link_parts = explode("|", $link);

			foreach ( $link_parts as $key => $link_part ) {
				if ( strpos( $link_part, 'url:' ) !== false ) {
					$href_attrs['href'] = 'href="' . esc_url( urldecode(explode( 'url:', explode('|', $link )[0])[1]) ) . '"';
				} elseif ( strpos( $link_part, 'title:' ) !== false ) {
					$href_attrs['title'] = 'title="' . explode( 'title:', $link_part )[1] . '"';
				} elseif ( strpos( $link_part, 'target:' ) !== false ) {
					$href_attrs['target'] = 'target="' . explode( 'target:', $link_part )[1] . '"';
				} elseif ( strpos( $link_part, 'rel:' ) !== false ) {
					$href_attrs['rel'] = 'rel="' . explode( 'rel:', $link_part )[1] . '"';
				}
			}
		}
		
		// Blurb for user
		if ( $blub_type == 'user' ) {
			// $additionals = array();
			$user_id = ( $blurb_user == 0 ) ? get_current_user_id() : $blurb_user;
			if ( $link_type == 'link_author' ) {
				$href_attrs = array(
					'href' => 'href="' . esc_url( get_author_posts_url( $user_id ) ) . '"'
				);
				$link_type = 'link_button';
			}
			
			if ( !is_user_logged_in() && $user_id == 0 ) {
				// Break the blurb for non logged-in users and no user chosen from settings
				return;
			}

			if ( empty( $image ) ) {
				$user_avatar = apply_filters( 'vc_baum_blurb_avatar', get_avatar( $user_id ) );
			}

			if ( !empty( $content ) ) {
				$defaults['content'] = $content;
			}

			$content = $this->vc_baum_blurb_user_data( $user_id, explode( ',', str_replace( ' ', '', $blurb_user_fields ) ), $defaults );
		} elseif ( $blub_type == 'icon' ) {
			$icon_class = isset( ${'icon_' . $icon_type} ) ? esc_attr( ${'icon_' . $icon_type} ) : $icon_fontawesome;
		}
		
		ob_start();
		?>
		<div class="vc_blurb <?= implode(' ', $custom_classes) ?>">
			<?= ( $link_type == 'link_blurb' ) ? '<a ' . implode(' ', $href_attrs) . ' >' : ''; ?>

				<?php if ( !empty( $image ) || !empty( $user_avatar ) || !empty( $icon_class ) ) { ?>
					<div class="blurb-media">
						<span <?= $blurb_styles_tag ?> class="<?= implode(' ', $blub_classes); ?>">
							<?= ( $link_type == 'link_icon_image' ) ? '<a ' . implode(' ', $href_attrs) . ' >' : ''; ?>
								<?php
								if ( !empty( $image ) ) {
									echo wp_get_attachment_image( $image, $image_size );
								} elseif ( !empty( $user_avatar ) ) {
									echo $user_avatar;
								} else {
									?>
									<i class="<?= $icon_class ?>" <?= $icon_styles_tag ?>></i>
									<?php
								}
								?>
							<?= ( $link_type == 'link_icon_image' ) ? '</a>' : ''; ?>
						</span>
					</div>
				<?php } ?>

				<?php if ( !empty( $content ) ) { ?>
					<div class="blurb-content">
						<?= wpb_js_remove_wpautop( $content, true ); ?>
					</div>
				<?php } ?>
			<?= ( $link_type == 'link_blurb' ) ? '</a>' : ''; ?>

			<?php
				if ( $link_type == 'link_button' && !empty( $button_link_text ) ) {
					$href_attrs['class'] = 'class="' . ( !empty( $button_classes ) ? $button_classes : 'btn vc_btn-baum-primary' ) . '"';
			?>
				<div class="blurb-actions">
					<a <?= implode(' ', $href_attrs); ?> ><?= $button_link_text; ?></a>
				</div>
			<?php } ?>
		</div>
		<?php
		return apply_filters( 'vc_baum_blurb_wrapper', ob_get_clean(), $atts );
	}

	/**
	 * Register FA from WPBakery
	 * 
	 * @version 1.0.4
	 */
	public function vc_baum_register_font_awesome_css() {
		wp_enqueue_style( 'vc_font_awesome_5_shims', vc_asset_url( 'lib/bower/font-awesome/css/v4-shims.min.css' ), array(), WPB_VC_VERSION );
		wp_enqueue_style( 'vc_font_awesome_5', vc_asset_url( 'lib/bower/font-awesome/css/all.min.css' ), array( 'vc_font_awesome_5_shims' ), WPB_VC_VERSION );

		wp_enqueue_style( 'vc_baum_bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' );
	}

	/**
	 * Get all users
	 * 
	 * @since 1.0.4
	 * @return $users	The users
	 */
	private function vc_baum_blurb_get_users() {
		$all_users = get_users();
		$the_users = array( __('Detectar usuario logueado', 'baumchild') => '0' );

		foreach ( $all_users as $user ) {
			$formatted_name = sprintf( '%1$s %2$s', $user->first_name, $user->last_name, $user->ID );
			$formatted_name = sprintf( '%1$s (%2$s)', ( strlen( $formatted_name ) > 2 ? $formatted_name : $user->display_name ), $user->ID );
			$the_users[ $formatted_name ] = $user->ID;
		}

		return $the_users;
	}

	/**
	 * Get user fields
	 * 
	 * @since 1.0.4
	 */
	private function vc_baum_blurb_user_fields() {
		return apply_filters( 'vc_baum_blurb_user_fields',
			array(
				'content (Bloque de contenido)', // Content field from blurb options
				'description (Biografía de usuario)',
				'formatted_name'
			) 
		);
	}

	/**
	 * Get user data by ID
	 * 
	 * @since 1.0.4
	 * @param int $user_id	User ID
	 * @param array $user_fields	The user fields added from Blurb options
	 * @param array $defaults	Default data to validate content
	 * @param bool $is_array	Check if response is returned as array or string
	 * @return mixed	The user data as array or string
	 */
	public function vc_baum_blurb_user_data( $user_id, $user_fields = array(), $defaults = array(), $is_array = false ) {
		$the_users_arr = $user_name = array();
		$the_users_str = $user_meta_value = '';

		if ( is_numeric( $user_id ) && !empty( $user_fields ) ) {
			$user = get_userdata( $user_id );

			foreach ( $user_fields as $key => $field ) {
				switch ( $field ) {					
					case 'formatted_name':
						$user_name[] = !empty( $user->first_name ) ? $user->first_name : $user->display_name;
						$user_name[] = !empty( $user->last_name ) ? $user->last_name : '';

						$user_meta_value = implode( ' ', $user_name );
					break;
					case 'content':
						$user_meta_value = isset( $defaults[$field] ) ? $defaults[$field] : '';
					break;
					default:
						// Meta fields
						$user_meta_value = get_user_meta( $user_id, $field, true );
					break;
				}
				
				if ( !empty( $user_meta_value ) ) {
					$meta_field = apply_filters( 'vc_baum_blurb_user_meta_value', sprintf( '<p class="user-%1$s">%2$s</p>', $field, $user_meta_value ), $user_meta_value, $field );
					$the_users_arr[ $field ] =  $meta_field;
					$the_users_str .=  $meta_field;
				}
			}
		}

		return $is_array ? $the_users_arr : $the_users_str;
	}
}

function vc_baum_blurb() {
	return new VC_Baum_Blurb();
}