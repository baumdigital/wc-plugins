## Version - 1.0.4 ##

## Implementación ##
- Mueva el contenido de functions y source donde corresponda en el tema child.
- Agregue el archivo SCSS a _sections.scss para los estilos: @import "sections/visual-composer/vc_baum-blurb";
- Llame el archivo del Baum Blurb en functions/vc-elements/custom-elements.php: require get_stylesheet_directory() . '/functions/vc-elements/baum/vc_baum-blurb.php';
- Llame la funcion que inicializa el Blurb en el constructor de la clase (__construct) en custom-elements.php: VC_BAUM_BLURB();
- DEV: Aplicar el uso de los filters en el archivo custom-elements.php.
- DEV: Evitar la edición del componente, a menos que sea algo custom del proyecto, en ese caso agregar al final de la versión "x". Ej: 1.0.4.x


## Changelog ##
* v1.0.4
- Registro de Font Awesome y Bootstrap desde vc_baum_register_font_awesome_css
- Compatibilidad para Usuario en Blurbs
- Métodos vc_baum_blurb_get_users, vc_baum_blurb_user_fields, vc_baum_blurb_user_data para manejo de datos de usuario
- DEV: filtro vc_baum_blurb_wrapper para sobreescribir el bloque de blurb completamente
- DEV: filtro vc_baum_blurb_user_fields para agregar campos custom según lo requiera el proyecto
- DEV: filtro vc_baum_blurb_user_meta_value para sobreescribir el valor que se imprime en los datos del usuario
- DEV: filtro vc_baum_blurb_avatar para quitar imagen en los datos del usuario


## DEMOS ##
/**
 * Filtering user fields
 * PSP Custom - Baum Blurbs
 * 
 * @version 1.0.4
 */
function vc_baum_blurb_user_fields( $default_fields ) {
	return wp_parse_args( array(
		'user_level',
		'codigo_colegiado',
		'user_verified'
	), $default_fields );
}
add_filter( 'vc_baum_blurb_user_fields', 'vc_baum_blurb_user_fields' );

/**
 * Filtering user custom value for field user_level
 * PSP Custom - Baum Blurbs
 * 
 * @version 1.0.4
 */
function vc_baum_blurb_user_meta_value( $element, $user_meta_value, $field ) {
	if ( $field == 'content' ) {
		ob_start();
		?>
		<p class="CUSTOM-CLASS">MY CUSTOM CONTENT</p>
		<?php
		$element = ob_get_clean();
	}

	return $element;
}
add_filter( 'vc_baum_blurb_user_meta_value', 'vc_baum_blurb_user_meta_value', 10, 3 );