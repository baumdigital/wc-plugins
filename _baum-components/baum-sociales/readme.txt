## Version - 1.0.3 ##

## Implementación ##
- Mueva el contenido de functions y source donde corresponda en el tema child.
- Agregue el archivo SCSS a _sections.scss para los estilos: @import "sections/common/baum-sociales";
- Llame el archivo del Baum Sociales para VC en functions/vc-elements/custom-elements.php: require get_stylesheet_directory() . '/functions/vc-elements/baum/vc_baum-sociales.php';
- Llame la funcion que inicializa los Sociales en el constructor de la clase (__construct) en custom-elements.php: VC_SOCIALES();
- Llame el archivo del Baum Sociales principal en includes.php: require get_stylesheet_directory() . '/functions/wp-baum-sociales.php';


## Changelog ##