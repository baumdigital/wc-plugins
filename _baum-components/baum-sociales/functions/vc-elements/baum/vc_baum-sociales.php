<?php
/**
** VC Baum Sociales
** Call this file in /functions/vc-elements/custom-elements.php
** then call function VC_SOCIALES in __construct()
**
** Requires function baumchild_sociales to get all sociales. Extension to show custom Baum Sociales from Customizer options.
** v1.0
**/
class VC_Baum_Sociales {
	function __construct() {
		// Element Mapping
		add_action('init', array($this, 'vc_baum_sociales_mapping'));

		// Element HTML
		add_shortcode('vc_baum_sociales', array($this, 'vc_baum_sociales_html'));
	}

	/**
	** Custom element to display Baum Sociales
	**/
	public function vc_baum_sociales_mapping() {
		// Stop all if VC is not enabled
		if (!defined('WPB_VC_VERSION')) { return; }

		$sociales = array(
			array(
				'type' => 'textfield',
				'heading' => __('Label', 'baumchild'),
				'param_name' => 'sociales_label',
				'admin_label' => true,
				'description' => __('Encabezado del listado de sociales', 'baumchild')
			),
			array(
				'type' => 'textfield',
				'heading' => __('Wrapper classes', 'baumchild'),
				'param_name' => 'sociales_wrapper_css',
				'admin_label' => true,
				'description' => __('Clases custom al contenedor de los sociales', 'baumchild')
			)
		);

		foreach (baumchild_sociales() as $key => $social) {
			$item = str_replace('baumchild_contacto_', '', $key);
			$message = __('Vacío. Editar opciones en Apariencia > Personalizar > Sociales Child', 'baumchild');
			$sociales[] = array(
				'type' => 'checkbox',
				'heading' => $social['label'],
				'param_name' => $key,
				'group' => ucfirst($item),
				'description' => !empty(get_theme_mod($key)) ? 'Current: ' . get_theme_mod($key) : $message,
				'value' => ''
			);
			$sociales[] = array(
				'type' => 'textfield',
				'heading' => sprintf(__('Etiqueta %s', 'baumchild'), ucfirst($item)),
				'param_name' => $item . '_label',
				'group' => ucfirst($item),
				'value' => ''
			);
			$sociales[] = array(
				'type' => 'checkbox',
				'heading' => __('Etiqueta a la derecha', 'baumchild'),
				'param_name' => $item . '_right',
				'group' => ucfirst($item),
				'description' => __('Posiciona la etiqueta a la derecha del valor', 'baumchild')
			);
			$sociales[] = array(
				'type' => 'checkbox',
				'heading' => __('Mostrar como enlace', 'baumchild'),
				'param_name' => $item . '_link',
				'group' => ucfirst($item),
				'value' => ''
			);
			$sociales[] = array(
				'type' => 'checkbox',
				'heading' => __('Mostrar solo ícono', 'baumchild'),
				'param_name' => $item . '_icon',
				'group' => ucfirst($item),
				'value' => '',
				'description' => __('Muestra ícono de librería Font Awesome', 'baumchild')
			);
		}	

		// Map the block with vc_map()
		vc_map(
			array(
				'name' => __('Baum Theme Sociales', 'baumchild'),
				'base' => 'vc_baum_sociales',
				'description' => __('Muestra los enlaces de sociales en el sitio', 'baumchild'),
				'category' => __('Baum Themes', 'baumchild'),
				'icon' => get_stylesheet_directory_uri() . '/functions/vc-elements/images/placeholder.jpg',
				'params' => $sociales
			)
		);
	}

	public function vc_baum_sociales_html($atts) {
		// Params extraction
		extract( $atts );
		$classes = 'vc_baum-sociales sociales-wrapper';
		$classes = (!empty($atts['sociales_wrapper_css'])) ? $classes . ' ' . $atts['sociales_wrapper_css'] : $classes;

		ob_start();
		?>
		<div class="<?= $classes ?>">
			<?php if (!empty($atts['sociales_label'])) : ?>
				<span class="baumsociales-label"><?= $atts['sociales_label'] ?></span>
			<?php endif; ?>
			<?php
			foreach ($atts as $key => $attr) {
				if (preg_match('/baumchild_/', $key)) {
					$item = str_replace('baumchild_contacto_', '', $key);
					$item_label = $atts[$item . '_label'];
					echo do_shortcode('[baumchild_social id="' . $item . '" is_link="' . $atts[$item . '_link'] . '" title="' . $atts[$item . '_label'] . '" is_right="' . $atts[$item . '_right'] . '" show_icon="' . $atts[$item . '_icon'] . '"]');
				}
			}
			?>
		</div>
		<?php
		return ob_get_clean();
	}
}

function vc_sociales() {
	if (!function_exists('baumchild_sociales')) 
		return;

	return new VC_Baum_Sociales();
}
