<?php
/**
 * Create a custom log file
 * Usage: Baum_Logger::baumlog_add( $message )
 * 
 * @version 1.0.1
 */
class Baum_Logger {
	public static $dirname;
	public static $formatted_file_name;

	/**
	 * Instance variable
	 *
	 * @var $instance The reference the *Singleton* instance of this class
	 */
	private static $instance;

	/**
	 * Returns the *Singleton* instance of this class.
	 *
	 * @return The|Baum_Logger $instance The *Singleton* instance.
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}		
		return self::$instance;
	}

	public function __construct() {
		// Creates the logfile directory 
		self::$formatted_file_name = 'baumlog_' . date_i18n('Y-m-d') . '.log';
		self::$dirname = wp_upload_dir()['basedir'] . '/baum_logs/';
		
		if (!file_exists( self::$dirname )) {
			wp_mkdir_p( self::$dirname );
		}
	}

	public static function baumlog_add($message = '') {
		if ( is_array( $message ) ) { 
			$message = json_encode( $message ); 
		}

		$file = fopen( self::$dirname . self::$formatted_file_name, "a" );
		fwrite( $file, date_i18n('Y-m-d h:i:s') . ": " . $message . "\n\n" );
		fclose( $file ); 
	}

	/**
	 * Clear all logs older than a defined number of days. Defaults to 30 days.
	 * Based on WC_Logger class
	 */
	public static function baumlog_clear_expired_logs() {
		$days = absint( apply_filters( 'baumlog_days_to_retain_logs', 30 ) );
		$timestamp = strtotime( "-{$days} days" );

		self::baumlog_delete_before_timestamp( $timestamp );
	}

	public static function baumlog_delete_before_timestamp( $timestamp = 0 ) {
		if ( ! $timestamp ) {
			return;
		}

		$log_files = self::baumlog_get_files();

		foreach ( $log_files as $log_file ) {
			$last_modified = filemtime( trailingslashit( self::$dirname ) . $log_file );

			if ( $last_modified < $timestamp ) {
				@unlink( trailingslashit( self::$dirname ) . $log_file );
			}
		}
	}

	/**
	 * Get all log files in the log directory.
	 *
	 * @since 3.4.0
	 * @return array
	 */
	public static function baumlog_get_files() {
		$files  = @scandir( self::$dirname );
		$result = array();

		if ( ! empty( $files ) ) {
			foreach ( $files as $key => $value ) {
				if ( ! in_array( $value, array( '.', '..' ), true ) ) {
					if ( ! is_dir( $value ) && strstr( $value, '.log' ) ) {
						$result[ sanitize_title( $value ) ] = $value;
					}
				}
			}
		}

		return $result;
	}

}
return Baum_Logger::get_instance();

/**
** Cron job init
**/
function baumchild_cleanup_logs() {
	$scheduled = wp_get_schedule('baumchild_cleanup_logs_hook');

	if ( !wp_next_scheduled('baumchild_cleanup_logs_hook') ) {
		wp_schedule_event(time() + ( 3 * HOUR_IN_SECONDS ), 'daily', 'baumchild_cleanup_logs_hook');
	}
}
add_action('wp', 'baumchild_cleanup_logs');

/**
 * Cron job task
 * 
 * @since 1.0.0
 * @version 1.0.1
 */
function baumchild_cleanup_logs_cronjob() {
	if (class_exists('Baum_Logger')) {
		Baum_Logger::baumlog_clear_expired_logs();
	}
}
add_action('baumchild_cleanup_logs_hook', 'baumchild_cleanup_logs_cronjob');
