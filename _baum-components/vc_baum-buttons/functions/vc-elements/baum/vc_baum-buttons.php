<?php
/**
 * VC Baum Buttons
 * Add custom buttons to Bakery
 * 
 * Call this file in /functions/vc-elements/custom-elements.php
 * then call function VC_BAUM_BUTTONS in __construct()
 * 
 * @version 1.0.0
 */
class VC_Baum_Buttons {
	function __construct() {
		add_action( 'vc_after_init', array( $this, 'vc_custom_button_colors' ) );

		add_action('admin_init', array( $this, 'baumchild_vc_admin_buttons_style' ) );
	}

	/**
	 * Custom buttons by Baum
	 * 
	 * @since 1.0.0
	 */
	function vc_custom_button_colors() {
		// Get current values stored in the color param in "Call to Action" element
		$param = WPBMap::getParam( 'vc_btn', 'color' );
		$param_style = WPBMap::getParam( 'vc_btn', 'style' );
		$param_shape = WPBMap::getParam( 'vc_btn', 'shape' );

		// New custom buttons
		$param['value'][__( 'Baum Primary button', 'baumchild' )] = 'baum-primary vc_btn-baum-primary';
		$param['value'][__( 'Baum Secondary button', 'baumchild' )] = 'baum-secondary vc_btn-baum-secondary';
		$param['value'][__( 'Baum Primary Light button', 'baumchild' )] = 'baum-primary-light vc_btn-baum-primary-light';
		$param['value'][__( 'Baum Secondary Light button', 'baumchild' )] = 'baum-secondary-light vc_btn-baum-secondary-light';
		$param_style['value'][__( 'Baum Default Theme', 'baumchild' )] = 'baum';
		$param_shape['value'][__( 'Baum Default Theme', 'baumchild' )] = 'baum';

		// Remove any colors you don't want to use.
		unset($param['value']['Classic Grey']);
		unset($param['value']['Classic Blue']);
		unset($param['value']['Classic Turquoise']);
		unset($param['value']['Classic Green']);
		unset($param['value']['Classic Orange']);
		unset($param['value']['Classic Red']);
		unset($param['value']['Classic Black']);
		unset($param['value']['Blue']);
		unset($param['value']['Turquoise']);
		unset($param['value']['Pink']);
		unset($param['value']['Violet']);
		unset($param['value']['Peacoc']);
		unset($param['value']['Chino']);
		unset($param['value']['Mulled Wine']);
		unset($param['value']['Vista Blue']);
		unset($param['value']['Black']);
		unset($param['value']['Grey']);
		unset($param['value']['Orange']);
		unset($param['value']['Sky']);
		unset($param['value']['Green']);
		unset($param['value']['Juicy pink']);
		unset($param['value']['Sandy brown']);
		unset($param['value']['Purple']);
		unset($param['value']['White']);

		// Finally "update" with the new values
		vc_update_shortcode_param( 'vc_btn', $param );
		vc_update_shortcode_param( 'vc_btn', $param_style );
		vc_update_shortcode_param( 'vc_btn', $param_shape );
	}
	
	/**
	 * Style for Bakery admin
	 * 
	 * @since 1.0.0
	 */
	function baumchild_vc_admin_buttons_style() {
		global $pagenow;
	
		if ( 'post.php' === $pagenow && isset( $_GET['post'] ) ) {
			$post_id = $_GET['post'];
			$post = get_post( $post_id );
			
			if ( vc_check_post_type( $post->post_type ) ) {
				?>
				<style>
					[class*="vc_btn-baum-primary"] {
						background-color: #612950 !important;
						color: #fff !important;
						border: 1px solid currentColor !important;
					}
					[class*="vc_btn-baum-secondary"] {
						background-color: #48776c !important;
						color: #fff !important;
						border: 1px solid currentColor !important;
					}
					[class*="vc_btn-baum-primary-light"] {
						background-color: #fff !important;
						color: #612950 !important;
						border: 1px solid currentColor !important;
					}
					[class*="vc_btn-baum-secondary-light"] {
						background-color: #fff !important;
						color: #48776c !important;
						border: 1px solid currentColor !important;
					}
				</style>
				<?php
			}
		}
	}
}

function vc_baum_buttons() {
	return new VC_Baum_Buttons();
}
