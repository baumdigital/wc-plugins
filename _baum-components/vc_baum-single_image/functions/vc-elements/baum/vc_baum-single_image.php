<?php
/**
** VC Baum Single Image
** Call this file in /functions/vc-elements/custom-elements.php
** then call function VC_BAUM_SINGLE_IMAGE in __construct()
**
** Extends default Single Image component from Visual composer
** v1.0
**/
class VC_Baum_Single_Image {
	function __construct() {
		// Element Remapping
		add_action('vc_after_init', array($this, 'vc_single_image_remapping'));
	}

	/**
	** Extend Single Image component
	**/
	function vc_single_image_remapping() {
		// Remove Params
		if (function_exists('vc_remove_param')) {
			vc_remove_param('vc_single_image', 'el_id');
		}

		// Add new Params
		$vc_single_image_new_params = array(
			array(
				'type' => 'checkbox',
				'class' => 'attr_bg_image',
				'heading' => __('Usar imagen como background?', 'baumchild'),
				'param_name' => 'attr_bg_image',
				'description' => __('Usar imagen como background?', 'baumchild'),
				'group' => esc_html__( 'Custom by Baum', 'baumchild' )
			),
			array(
				'type' => 'dropdown',
				'value' => array(
					__('None', 'baumchild') => '',
					__('Image Left', 'baumchild') => 'image_left',
					__('Image Right', 'baumchild') => 'image_right',
				),
				'class' => 'row_image_position',
				'heading' => __('Full width row & Image position', 'baumchild'),
				'param_name' => 'row_image_position',
				'description' => __('Posiciona la imagen en una fila de texto + imagen.', 'baumchild'),
				'dependency' => array(
					'element' => 'attr_bg_image',
					'value' => 'true',
				),
				'group' => esc_html__('Custom by Baum', 'baumchild'),
			)
		);

		vc_add_params('vc_single_image', $vc_single_image_new_params);
	}
}

function vc_baum_single_image() {
	return new VC_Baum_Single_Image();
}