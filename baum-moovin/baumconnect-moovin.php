<?php
class BAUM_MOOVIN_VALIDACION {
	public function __construct() {

		$this->init();
	}

	public function init() {
		$data = $_POST;
		$result = array();

		if(isset($data) && !empty($data['ws_endpoint'])) {
			foreach($data as $key => $data) {
				$this->$key = $data;
			}
			
			switch ($this->ws_method) {
				case 'baumconnect_get_token':
					$result = $this->baumconnect_get_token();
				break;
				case 'baumconnect_get_estimation':
					$result = $this->baumconnect_get_estimation();
				break;
				// case 'baumconnect_registro_envio':
				// 	$result = $this->baumconnect_registro_envio();
				// break;
				// case 'baumconnect_testing':
				// 	$result = $this->test_url();
				// break;
				
				default:
					$result[] = 'Método no disponible';
				break;
			}
		} else {
			$result['message'] = 'No data';
		}

		echo json_encode($result);
	}

	public function test_url() {
		return $this->ws_endpoint;
	}

	public function baumconnect_get_token() {
		$func_suffix = '/moovinApiWebServices-1/rest/api/moovinEnterprise/partners/login';
		$body = array(
			'username' => $this->ws_username,
			'password' => $this->ws_password
		);
		$response = $this->baumconnect_ws_query($func_suffix, json_encode($body));

		return $response;
	}

	/**
	** Apply calculated price from moovin to shipping method
	**/
	public function baumconnect_get_estimation($coords, $vehicle) {
		$func_suffix = '/moovinApiWebServices-1/rest/api/ecommerceExternal/estimation';
		$headers[] = 'token: ' . $this->baum_moovin_get_token();
		$collect_coords = explode(', ', $this->point_to_collect);
		$delivery_coords = explode(', ', $coords);
		$estimation = array();
		// $formatted_products = $this->baum_moovin_get_formatted_products();
		// $vehicle = (isset($formatted_products['apply_car_service']) && $formatted_products['apply_car_service'] == 1) ? 'car' : $vehicle;
		$vehicle = $vehicle;

		if(!empty($collect_coords) && !empty($delivery_coords)) {
			$body = array(
				'vehicle' => $vehicle, // car
				'pointCollect' => array(
					'latitude' => $collect_coords[0],
					'longitude' => $collect_coords[1]
				),
				'pointDelivery' => array(
					'latitude' => $delivery_coords[0],
					'longitude' => $delivery_coords[1]
				),
				'listProduct' => $formatted_products['items'],
				'ensure' => $this->moovin_ensure
			);
			$response = $this->baum_moovin_ws_query($func_suffix, json_encode($body), $headers);

			if($this->baum_moovin_check_response_status($response)) {
				WC()->session->set('moovin_estimation_data', array(
					'id_estimation' => $response['idEstimation'],
					'id_delivery' => $response['optionService'][0]['id'],
					'vehicle' => $vehicle
				));

				$estimation = $response['optionService'];
				WC()->session->set('moovin_estimation_' . $vehicle, json_encode($estimation));
			} else {
				$estimation = $response['message'];
			}
		} else {
			$this->baum_moovin_log_message(__('Coords are required', $this->text_domain));
		}

		return $estimation;
	}

	/**
	** Main method to connect the API
	**/
	public function baumconnect_ws_query($function, $body, $headers = array(), $method = 'POST') {
		$url = $this->ws_endpoint . $function;
		$initial_headers = $this->baumconnect_api_headers();
		$headers = array_merge($headers, $initial_headers);

		if(!empty($url)) {
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return response instead of outputting
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			// 	'Content-Type: application/json',
			// 	// 'Content-Length: ' . strlen($body)
			// ));
			$result = curl_exec($ch); // execute the POST request
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);

			$response = json_decode($result, true);
		} else {
			$response['error'] = 'No hay conexión. Requiere URL de conexión de Baum para salida a CCR (BaumConnect URL)';
		}

		return $response;
	}

	public function baumconnect_api_headers() {
		$headers = array(
			'Content-Type: application/json'
		);

		return $headers;
	}
}
$obj = new BAUM_MOOVIN_VALIDACION();
// var_dump($obj->test_url());
// var_dump($obj->baumconnect_get_token());
// var_dump($obj->baumconnect_get_estimation('', 'motorcycle'));